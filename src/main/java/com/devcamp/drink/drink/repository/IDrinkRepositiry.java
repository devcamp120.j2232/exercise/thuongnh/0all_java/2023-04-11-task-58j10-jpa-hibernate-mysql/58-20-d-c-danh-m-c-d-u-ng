package com.devcamp.drink.drink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drink.drink.model.CDrink;

public interface IDrinkRepositiry extends JpaRepository<CDrink, Long>{
    
}
