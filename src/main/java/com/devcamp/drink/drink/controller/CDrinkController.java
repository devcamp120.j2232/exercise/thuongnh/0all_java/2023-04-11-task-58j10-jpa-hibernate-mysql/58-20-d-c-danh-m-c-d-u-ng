package com.devcamp.drink.drink.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drink.drink.model.CDrink;
import com.devcamp.drink.drink.repository.IDrinkRepositiry;


@CrossOrigin
@RestController
public class CDrinkController {

    @Autowired
    IDrinkRepositiry pDrinkRepositiry;


    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        
        try {
            List<CDrink> drinks = new ArrayList<CDrink>();
            pDrinkRepositiry.findAll().forEach(drinks::add);
            return new ResponseEntity<> (drinks, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<> (null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
